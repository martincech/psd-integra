v 1.2.1
   - Nastaven� funkce kl�ves

v 1.1.1
   - Ukl�d�n� dat do souboru po ka�d� zm�n�

v 1.1.0
   - P�id�n� podpory bluetooth v�hy
   - Stanovena minim�ln� v�ha p�ij�man� z BT v�hy (10g)
   - odd�lova� des. ��sla se ��d� podle jazyka UI a ne podle syst�mu

v 1.0.0
   - Zaveden� verzov�n� softwaru
   - Jm�no souboru a zadavatele: povoleny jsou pouze p�smena, ��slice a podtr��tko
   - P�i vyvol�n� nab�dky z menu nedoch�z� k ukon�en� p�edchoz� aktivity