using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Views;
using Android.Widget;
using PSD.Core.Models;
using PSD.Core.Repository.Interfaces;
using PSD.Droid.Activities;
using PSD.Droid.ViewHolders;

namespace PSD.Droid.Adapters
{
   public class KeySettingsAdapter : BaseAdapter<CommandKey>
   {
      #region Private fields

      private IEnumerable<CommandKey> keySettingsList;
      private readonly Activity context;

      #endregion

      #region Constructor

      public KeySettingsAdapter(Activity activity)
      {
         context = activity;
         UpdateData();
      }

      #endregion

      public void UpdateData()
      {
         keySettingsList = ((BaseActivity<ISettings>)context).ViewModel.Keys;
      }

      #region Overrides of BaseAdapter

      public override CommandKey this[int position]
      {
         get { return keySettingsList.ElementAt(position); }
      }

      public override int Count
      {
         get { return keySettingsList.Count(); }
      }

      public override long GetItemId(int position)
      {
         return this[position].KeyCode;
      }

      public override View GetView(int position, View convertView, ViewGroup parent)
      {
         KeySettingsItemViewHolder holder = null;
         var view = convertView;
         if (view != null)
         {
            holder = view.Tag as KeySettingsItemViewHolder;
         }
         if (holder == null)
         {
            view = context.LayoutInflater.Inflate(Resource.Layout.KeySettingsItem, null);
            holder = new KeySettingsItemViewHolder
            {
               Command = view.FindViewById<TextView>(Resource.Id.commandTextView),
               KeyCode = view.FindViewById<TextView>(Resource.Id.keyCodeTextView),
               WarningImage = view.FindViewById<ImageView>(Resource.Id.warningImageView)
            };
         }

         // set values
         holder.Command.Text = this[position].Text;

         var code = "";
         if (this[position].KeyCode != -1)
         {
            var keycodeString = KeyEvent.KeyCodeToString((Keycode)this[position].KeyCode);
            code = keycodeString.Replace("KEYCODE_", "");
            holder.KeyCode.Visibility = ViewStates.Visible;
            holder.WarningImage.Visibility = ViewStates.Invisible;
         }
         else
         {
            holder.KeyCode.Visibility = ViewStates.Invisible;
            holder.WarningImage.Visibility = ViewStates.Visible;
         }
         holder.KeyCode.Text = code;
         return view;
      }

      #endregion

      
   }
}