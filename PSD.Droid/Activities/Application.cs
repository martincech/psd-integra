using System;
using Android.App;
using Android.Content.Res;
using Android.Runtime;
using Java.Util;
using Ninject;
using PSD.Core;
using PSD.Core.Repository;
using PSD.Core.Repository.Interfaces;
using PSD.Core.ViewModels;
using PSD.Droid.Scale;

namespace PSD.Droid.Activities
{
   [Application(Theme = "@android:style/Theme.Holo.Light.NoActionBar")]
   public class App : Application
   {
      public App(IntPtr javaReference, JniHandleOwnership transfer) :
         base(javaReference, transfer)
      {
      }

      public override void OnCreate()
      {
         base.OnCreate();
         
         InitIoc();
         SetLanguage();
         LoadSettings();
         InitRepository();
      }

      #region Private helpers

      private void InitIoc()
      {
         IKernel ninjectKernel = new StandardKernel();
         IocContainer.Container = ninjectKernel;

         //Models
         ninjectKernel.Bind<ICagesRepository>().To<CagesCsvRepository>().InSingletonScope();
         ninjectKernel.Bind<ISettings>().To<SettingsService>().InSingletonScope();
         ninjectKernel.Bind<ScaleService>().To<ScaleService>().InSingletonScope();

         //ViewModels
         IocContainer.Container.Bind<CagesViewModel>().ToSelf();
      }

      private void LoadSettings()
      {
         // Load settings
         var settings = IocContainer.Container.Get<ISettings>();
         settings.Load(BaseContext);
         Program.DbFolder = settings.LastFileFolder;
         Program.DbFileName = settings.LastFileName;
         Program.PickerName = settings.PickerName;
      }

      private void InitRepository()
      {
         // Load data from file
         var repository = (CagesCsvRepository) IocContainer.Container.Get<ICagesRepository>();
         var data = Program.LoadDb();
         repository.Parse(data);
      }

      /// <summary>
      /// Some of saved data can be localizable, therefore we must set language first
      /// </summary>
      private void SetLanguage()
      {
         // set application language
         var locale = new Locale(Settings.CultureName);
         Locale.Default = locale;
         var config = new Configuration {Locale = locale};
         BaseContext.Resources.UpdateConfiguration(config, BaseContext.Resources.DisplayMetrics);
      }

      #endregion
   }
}