using System;
using System.Linq;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using PSD.Core.Enums;
using PSD.Core.Repository.Interfaces;
using PSD.Droid.Adapters;

namespace PSD.Droid.Activities
{
   [Activity(Label = "KeySettingsActivity")]
   public class KeySettingsActivity : BaseActivity<ISettings>
   {
      #region Private fields

      private int changeKeyPosition;
      private ListView keyListView;

      #endregion

      protected override void OnCreate(Bundle bundle)
      {
         base.OnCreate(bundle);
         SetContentView(Resource.Layout.KeySettings);

         var backButton = FindViewById<Button>(Resource.Id.backButton);
         var defaultSettingsButton = FindViewById<Button>(Resource.Id.defaultSettingsButton);

         backButton.Click += BackButtonOnClick;
         defaultSettingsButton.Click += DefaultSettingsButtonOnClick;

         keyListView = FindViewById<ListView>(Resource.Id.KeyListView);
         keyListView.Adapter = new KeySettingsAdapter(this);
         keyListView.ItemClick += KeyListViewOnItemClick;
      }

      #region Overrides of BaseActivity<ISettings>

      protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
      {
         base.OnActivityResult(requestCode, resultCode, data);      
         switch (requestCode)
         {
            case (int)RequestE.PressedKeyCode:
               // When KeyChangeActivity returns pressed key
               if (resultCode == Result.Ok && TryUpdateList(data))
               {
                  SaveKeySettings();
               }
               break;
         }
      }

      #endregion

      #region Private helpers

      /// <summary>
      /// Check if new key is not used in another command and assigned 
      /// it to them.
      /// </summary>
      /// <param name="data"></param>
      /// <returns>true - key wasn't used yet and update was successful</returns>
      private bool TryUpdateList(Intent data)
      {
         var keycode = data.Extras.GetInt(KeyChangeActivity.KEY_CODE_PRESSED);
         if (!CanUseKeyCode(keycode, changeKeyPosition))
         {
            UseForceKeycodeDialog(keycode);
            return false;
         }
         UpdateList(keycode);
         return true;
      }

      /// <summary>
      /// Assigned new keycode to selected command and update view.
      /// </summary>
      /// <param name="keycode"></param>
      private void UpdateList(int keycode)
      {
         ViewModel.Keys.ElementAt(changeKeyPosition).KeyCode = keycode;
         keyListView.InvalidateViews();
      }

      /// <summary>
      /// Display window and check if user want used keycode,
      /// which is already assigned to another command. If he chooses yes,
      /// old command will be without keycode.
      /// </summary>
      /// <param name="keycode"></param>
      private void UseForceKeycodeDialog(int keycode)
      {
         var alert = new AlertDialog.Builder(this);
         alert.SetTitle(Resources.GetString(Resource.String.Warning));
         alert.SetPositiveButton(Resources.GetString(Resource.String.ButtonOK), (o, e) =>
         {
            // remove keycode from previous used command and update new command
            var item = ViewModel.Keys.First(f => f.KeyCode == keycode);
            item.KeyCode = -1;
            UpdateList(keycode);
            SaveKeySettings();
         });
         alert.SetNegativeButton(Resources.GetString(Resource.String.Back), (o, e) => { });
         alert.SetMessage(Resources.GetString(Resource.String.KeycodeIsUsed));
         alert.Show();
      }

      /// <summary>
      /// Check if keycode is used for another command.
      /// </summary>
      /// <param name="code">new added keycode</param>
      /// <param name="itemPosition">command position at list</param>
      /// <returns>true - command is not used yet</returns>
      private bool CanUseKeyCode(int code, int itemPosition)
      {
         for (var i = 0; i < ViewModel.Keys.Count(); i++)
         {
            if (ViewModel.Keys.ElementAt(i).KeyCode == code && i != itemPosition)
            {
               return false;
            }
         }
         return true;
      }

      /// <summary>
      /// Save key settings.
      /// </summary>
      private void SaveKeySettings()
      {
         ViewModel.Save(BaseContext);
      }

      /// <summary>
      /// List item click event.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="arg"></param>
      private void KeyListViewOnItemClick(object sender, AdapterView.ItemClickEventArgs arg)
      {
         changeKeyPosition = arg.Position;
         ShowKeyChangeDialog();
      }

      /// <summary>
      /// Show dialog and waiting for key press.
      /// Result is in OnActivityResult method.
      /// </summary>
      private void ShowKeyChangeDialog()
      {
         var serverIntent = new Intent(this, typeof (KeyChangeActivity));
         StartActivityForResult(serverIntent, (int) RequestE.PressedKeyCode);
      }

      /// <summary>
      /// Dialog for set default settings.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="eventArgs"></param>
      private void DefaultSettingsButtonOnClick(object sender, EventArgs eventArgs)
      {
         var alert = new AlertDialog.Builder(this);
         alert.SetPositiveButton(Resources.GetString(Resource.String.ButtonOK), (o, e) => { SetDefaultKeySettings(); });
         alert.SetNegativeButton(Resources.GetString(Resource.String.Back), (o, e) => { });
         alert.SetMessage(Resources.GetString(Resource.String.DefaultKeySettings));
         alert.Show();
      }

      /// <summary>
      /// Load default keys settings.
      /// </summary>
      private void SetDefaultKeySettings()
      {
         ViewModel.LoadDefault();
         ((KeySettingsAdapter)keyListView.Adapter).UpdateData();
         keyListView.InvalidateViews();
      }

      /// <summary>
      /// Back button click event.
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="eventArgs"></param>
      private void BackButtonOnClick(object sender, EventArgs eventArgs)
      {
         Finish();
      }

      #endregion
   }
}