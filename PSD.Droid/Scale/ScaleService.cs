using System;
using Android.App;
using Android.Bluetooth;
using Android.OS;
using Java.IO;
using Java.Util;
using PSD.Core.EggScale;
using PSD.Core.EggScale.Interfaces;
using Message = PSD.Core.EggScale.Interfaces.Message;
using State = PSD.Core.EggScale.Interfaces.State;

namespace PSD.Droid.Scale
{
   public class ScaleService : IEggScale
   {
      #region Fields and properties

      private BluetoothSocket _mSocket;
      private State _state;
      private Handler _handler;
      private Protocol _service;

      public string ConnectedDeviceName { get; private set; }
      public string ConnectedDeviceAddress { get; private set; }

      // Name for the SDP record when creating server socket
      public const string Name = "EggScale";

      // Key names received from the scale Handler
      public const string DeviceName = "device_name";
      public const string Toast = "toast";
      public const string CurrentWeight = "weight";

      // standard UUID for SPP
      public static UUID UuidUniverseProfile = UUID.FromString("00001101-0000-1000-8000-00805f9b34fb");

      #endregion

      #region Public interfaces

      public ScaleService()
      {
         _state = State.None;
         Handler = new ScaleHandler();
      }

      public State State
      {
         get { return _state; }
         private set
         {
            _state = value;
            Handler.ObtainMessage((int)Message.StateChange, (int)_state).SendToTarget();
         }
      }

      public Handler Handler { get { return _handler; } private set { _handler = value; } }

      /// <summary>
      /// Start the ConnectThread to initiate a connection to a remote device.
      /// </summary>
      /// <param name='device'>The BluetoothDevice to connect.</param>
      /// <param name="address">device address</param>
      public bool Connect(BluetoothDevice device, string address)
      {
         if (address.Equals(ConnectedDeviceAddress))
         {
            UpdateHandler(Message.Toast, Application.Context.Resources.GetString(Resource.String.AlreadyConnected));
            return true;
         }

         State = State.Connecting;
         if (OpenDeviceConnection(device))
         {
            UpdateHandler(Message.DeviceName, device.Name);
            State = State.Connected;
            Start();

            ConnectedDeviceName = device.Name;
            ConnectedDeviceAddress = address;
            return true;
         }
         return false;
      }

      /// <summary>
      /// Indicate that the connection was lost and notify the UI Activity.
      /// </summary>
      public void ConnectionLost()
      {
         State = State.Listen;

         // Send a failure message back to the Activity
         UpdateHandler(Message.Toast, Application.Context.Resources.GetString(Resource.String.ConnectionLost));
      }

      /// <summary>
      /// Indicate that the connection attempt failed and notify the UI Activity.
      /// </summary>
      public void ConnectionFailed()
      {
         State = State.Listen;

         // Send a failure message back to the Activity
         UpdateHandler(Message.Toast, Application.Context.Resources.GetString(Resource.String.ConnectError));
      }

      #endregion

      #region Private helpers

      private bool OpenDeviceConnection(BluetoothDevice btDevice)
      {
         try
         {
            if (ConnectedDeviceAddress != null && !ConnectedDeviceAddress.Equals(""))
            {  // check previous connection and potentially close it
               Stop();
            }
            //getting socket from specific device
            _mSocket = btDevice.CreateRfcommSocketToServiceRecord(UuidUniverseProfile);
            //blocking operation, please note
            _mSocket.Connect();
            _service = new Protocol(_mSocket.InputStream, _mSocket.OutputStream);
            _service.EggWeighted += ServiceOnEggWeighted;
            return true;
         }
         catch (IOException)
         {
            //close all
            btDevice.Dispose();
            ConnectionFailed();
            return false;
         }
      }

      private void ServiceOnEggWeighted(object sender, CommandArgs commandArgs)
      {
         if (commandArgs.State == State.Error)
         {
            ConnectionLost();
         }
         else if (commandArgs.Weight > 0)
         {
            InvokeEvent(commandArgs.Weight);   
         }
      }

      private void InvokeEvent(double weight)
      {
         // Send weight back to the Activity
         //UpdateHandler(Message.Read, weight.ToString());
         if (EggWeighted != null)
         {
            EggWeighted(this, new CommandArgs(weight));
         }
      }

      private void UpdateHandler(Message message, string text)
      {
         var caption = "";
         switch (message)
         {
            case Message.Read:
               caption = CurrentWeight;
               break;
            case Message.Toast:
               caption = Toast;
               break;
            case Message.DeviceName:
               caption = DeviceName;
               break;
         }

         var msg = Handler.ObtainMessage((int)message);
         var bundle = new Bundle();
         bundle.PutString(caption, text);
         msg.Data = bundle;
         Handler.SendMessage(msg);
      }

      #endregion

      #region Implementation of IEggScale

      public event EventHandler<CommandArgs> EggWeighted;

      public void Tare()
      {
         _service.Tare();
      }

      public void Backlight()
      {
         _service.Backlight();
      }

      public void Calibration()
      {
         _service.Calibration();
      }

      public void Count()
      {
         _service.Count();
      }

      public void ChangeUnit()
      {
         _service.ChangeUnit();
      }

      public void Start()
      {
         if (_service != null)
         {
            _service.Start();
         }
      }

      public void Stop()
      {
         if (_service != null)
         {
            _service.Stop();   
         }
         if (_mSocket != null)
         {
            _mSocket.Close();
         }
         ConnectedDeviceName = "";
         ConnectedDeviceAddress = "";
         State = State.Listen;
      }

      #endregion
   }
}