﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using PSD.Core;
using PSD.Core.Enums;
using PSD.Core.Models;
using PSD.Core.Repository;
using PSD.Core.Repository.Interfaces;
using PSD.Core.ViewModels;

namespace PSD.Tests.UnitTests
{
   [TestClass]
   public class CagesViewModelTests
   {
      private static CagesViewModel _vm;

      [ClassInitialize]
      public static void Init(TestContext context)
      {
         IKernel ninjectKernel = new StandardKernel();
         IocContainer.Container = ninjectKernel;
         IocContainer.Container.Bind<ICagesRepository>().To<CagesSampleData>();
         IocContainer.Container.Bind<CagesViewModel>().ToSelf();

         _vm = IocContainer.Container.Get<CagesViewModel>();
      }

      [TestMethod]
      public void NextCageNumber()
      {
         var nextNumber = _vm.FreeCageNumber;
         if (_vm.Cages.Any(cage => cage.CageNumber == nextNumber))
         {
            throw new Exception("Cage number is not unique");
         }
      }

      [TestMethod]
      public void AddCage()
      {
         var count = _vm.Cages.Count();

         var cage = new Cage {CageNumber = _vm.FreeCageNumber};
         var isAdded = _vm.AddCage(cage);

         Assert.IsTrue(isAdded);
         Assert.AreEqual(count + 1, _vm.Cages.Count());
      }

      [TestMethod]
      public void UpdateCage()
      {
         AddCage();
         var cage = _vm.Cages.Last();

         var nextNumber = _vm.FreeCageNumber;
         cage.CageNumber = nextNumber;
         cage.Clear();
         var recOk = new CageRecord
         {
            Count = 9,
            Type = ParameterE.ParameterOk
         };
         cage.Add(recOk);
         var recDead = new CageRecord
         {
            Type = ParameterE.ParameterDead
         };
         cage.Add(recDead);

         Assert.IsTrue(_vm.UpdateCage(cage));
         var addedCage = _vm.Cages.FirstOrDefault(c => c.CageNumber == nextNumber);
         Assert.IsNotNull(addedCage);
         Assert.AreEqual(cage.Records.Count(), addedCage.Records.Count());
         Assert.AreEqual(9, addedCage.Records.First(r => r.Type == ParameterE.ParameterOk).Count);
      }

      [TestMethod]
      public void DeleteCage_ByWholeCage()
      {
         AddCage();
         var cage = _vm.Cages.Last();
         var count = _vm.Cages.Count();

         Assert.IsTrue(_vm.DeleteCage(cage));
         Assert.AreEqual(count - 1, _vm.Cages.Count());
         Assert.IsNull(_vm.Cages.FirstOrDefault(c => c.CageNumber == cage.CageNumber));
      }

      [TestMethod]
      public void DeleteCage_ByCageNumber()
      {
         AddCage();
         var cage = _vm.Cages.Last();
         var count = _vm.Cages.Count();

         Assert.IsTrue(_vm.DeleteCage(cage.CageNumber));
         Assert.AreEqual(count - 1, _vm.Cages.Count());
         Assert.IsNull(_vm.Cages.FirstOrDefault(c => c.CageNumber == cage.CageNumber));
      }

      [TestMethod]
      public void MultipleOperation()
      {
         for (var i = _vm.Cages.Count() - 1; i >= 0; i--)
         {
            Assert.IsTrue(_vm.DeleteCage(_vm.Cages.ElementAt(i)));
         }
         Assert.AreEqual(0, _vm.Cages.Count());

         AddCage();
         AddCage();
         Assert.AreEqual(2, _vm.Cages.Count());
         var c = _vm.Cages.FirstOrDefault();
         Assert.IsNotNull(c);
         var cageNumber = c.CageNumber;
         c.CageNumber++;
         _vm.UpdateCage(c);

         Assert.AreNotEqual(cageNumber, _vm.Cages.First().CageNumber);
      }
   }
}
