﻿using System;

namespace PSD.Core.EggScale.Interfaces
{
   public class CommandArgs : EventArgs
   {
      public double Weight;
      public State State;

      public CommandArgs(double value)
      {
         Weight = value;
         State = State.Ok;
      }
   }
}
