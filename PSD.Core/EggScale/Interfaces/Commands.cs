﻿namespace PSD.Core.EggScale.Interfaces
{
   public enum Commands
   {
      DefaulState = 27,       //1Bh
      Print = 112,            //70h
      Calibration = 113,      //71h
      Count = 114,            //72h
      UnitConversion = 115,   //73h
      Tare = 116,             //74h
      Backlight = 117         //75h
   }
}
