using System.Collections.Generic;
using System.Linq;
using PSD.Core.Enums;
using Repository;

namespace PSD.Core.Models
{
   public class Cage : IRepository<CageRecord>
   {
      public int CageNumber;
      private List<CageRecord> cageRecords;
      public IEnumerable<CageRecord> Records { get { return cageRecords; } }
      public Cage()
      {
         cageRecords = new List<CageRecord>();
      }

      #region Implementation of IRepository<CageRecord>

      public bool Add(CageRecord o)
      {
         if (cageRecords.Contains(o))
         {
            return false;
         }
         cageRecords.Add(o);
         SortRecords();
         return true;
      }

      public bool Update(CageRecord o)
      {
         var recordAt = cageRecords.IndexOf(o);
         if (recordAt < 0)
         {
            return false;
         }
         cageRecords[recordAt] = o;
         SortRecords();
         return true;
      }

      public bool Delete(CageRecord o)
      {
         if (!cageRecords.Contains(o))
         {
            return false;
         }

         if (o.Type == ParameterE.ParameterOk)
         {  // OK parameter isn't deleted, only set to zero
            o.Count = 0;
            return true;
         }

         cageRecords.Remove(o);
         SortRecords();
         return true;
      }

      public IQueryable<CageRecord> All()
      {
         return cageRecords.AsQueryable();
      }

      /// <summary>
      /// Sort cageRecords by order in <see cref="ParameterE"/>.
      /// </summary>
      private void SortRecords()
      {
         cageRecords = cageRecords.OrderBy(r => r.Type).ToList();
      }

      #endregion

      public void Clear()
      {
         cageRecords.Clear();
      }
   }
}