﻿using System.Collections.Generic;
using PSD.Core.Models;
using Repository;

namespace PSD.Core.Repository.Interfaces
{
   public interface ICagesRepository : IRepository<Cage>
   {
      int GetFreeCageNumber();
      IEnumerable<Cage> Cages { get; }
   }
}
