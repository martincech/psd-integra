﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using PSD.Core.Helpers;
using PSD.Core.Models;

namespace PSD.Core.Repository
{
   public class CagesCsvParser
   {
      #region Fields and properties

      public char Delimiter { get; set; }
      private const int CagenumberIndex = 0;
      private const int CountIndex = 1;
      private const int ParameterIndex = 2;
      private StreamReader Reader { get; set; }
      private const int WeightIndex = 3;
      private string _separator;                // decimal separator

      #endregion

      #region Constructors

      public CagesCsvParser(byte[] byteArray, char delimiter)
      {
         var stream = new MemoryStream(byteArray);
         Reader = new StreamReader(stream);
         Delimiter = delimiter;
      }

      #endregion

      /// <summary>
      /// Parse data from <see cref="Reader"/>.
      /// </summary>
      /// <returns>list of cages</returns>
      public IEnumerable<Cage> Parse()
      {
         var culture = new CultureInfo(Settings.CultureName);
         _separator = culture.NumberFormat.NumberDecimalSeparator;

         var cages = new List<Cage>();
         var lastCageNumber = -1;
         while (!Reader.EndOfStream)
         {
            var line = Reader.ReadLine();
            if (line == null) continue;
            
            var values = line.Split(Delimiter);     
            int cageNumber, count;
            var parameter = values.Count() >= ParameterIndex + 1 ? values[ParameterIndex] : "";
            var weightRaw = values.Count() == WeightIndex + 1 ? values[WeightIndex] : "";

            if (parameter.Equals(Properties.Resources.Header_Parameter))
            {
               parameter = "";
            }

            var parsingList = new List<bool>
            {
               int.TryParse(values[CagenumberIndex], out cageNumber),
               int.TryParse(values[CountIndex], out count),
               !parameter.Equals("")
            };

            if (!parsingList.Contains(true))
            {  // parsing error (header or missing data => skip to next line)
               continue;
            }

            if (parsingList[CagenumberIndex])
            {
               lastCageNumber = cageNumber;
            }
            else
            {
               cageNumber = lastCageNumber;
            }

            double w;   // parse weight
            if (!double.TryParse(CheckDecimalSeparator(weightRaw), NumberStyles.Float, culture, out w))
            {
               w = -1;
            }

            CageRecord record = null;
            if (!parameter.Equals("") || parsingList[CountIndex])
            {
               record = new CageRecord
               {
                  Count = count,
                  Type = ParameterEToString.ConvertFromString(parameter),
                  Weight = w
               };
            }
            // Add cage to list or parameter to existing cage
            var cage = cages.FirstOrDefault(c => c.CageNumber == cageNumber);
            if (cage != null)
            {
               cage.Add(record);
            }
            else
            {
               var newCage = new Cage {CageNumber = cageNumber};
               if (record != null) newCage.Add(record);
               cages.Add(newCage);
            }
         }
         return cages;
      }

      /// <summary>
      /// Check and replace decimal separator with program culture.
      /// </summary>
      /// <param name="weight">weight</param>
      /// <returns>value with decimal separator of program culture</returns>
      private string CheckDecimalSeparator(string weight)
      {
         var oldSep = ".";
         var newSep = ",";
         if (_separator == oldSep)
         {
            oldSep = newSep;
            newSep = _separator;
         }
         return weight.Replace(oldSep, newSep);         
      }

   }
}
