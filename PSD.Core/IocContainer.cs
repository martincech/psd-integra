using Ninject;

namespace PSD.Core
{
   public class IocContainer
   {
      public static IKernel Container { get; set; }
   }
}